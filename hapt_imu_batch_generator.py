import numpy as np
import pandas as pd 
from random import randint
from keras.utils import to_categorical
import keras
from os.path import expanduser
#get actual home path for current user
home = expanduser("~")


ucihapt_datapath = home+"/python/data/HAPT_Dataset/"


#def get_test_uuids():



def get_steps_per_epoch(uuids):
	gt_df = pd.read_csv(ucihapt_datapath+"RawData/labels.txt",sep="\s",engine='python',names=['EXP_ID','USER_ID','LABEL','START','END'])
	#exclude other uuids
	print( gt_df.head() )
	filtered_df = pd.DataFrame(columns=['EXP_ID','USER_ID','LABEL','START','END'])
	for uuid in uuids:
		data_uuid = gt_df[ gt_df['USER_ID'] == uuid ]
		filtered_df = pd.concat([filtered_df,data_uuid], ignore_index=True)

	print( filtered_df.head() )
	#print("all data: ",len(gt_df))
	#print("only uuids: ", len(filtered_df))
	df_wal_all = filtered_df[filtered_df['LABEL'] == 1 ] #WALKING = 1
	df_sup_all = filtered_df[filtered_df['LABEL'] == 2 ] #
	df_sdo_all = filtered_df[filtered_df['LABEL'] == 3 ] #
	df_sit_all = filtered_df[filtered_df['LABEL'] == 4 ] #
	df_sta_all = filtered_df[filtered_df['LABEL'] == 5 ] #
	df_lay_all = filtered_df[filtered_df['LABEL'] == 6 ] #	
	df_sta2sit_all = filtered_df[filtered_df['LABEL'] == 7 ] #
	df_sit2sta_all = filtered_df[filtered_df['LABEL'] == 8 ] #
	df_sit2lay_all = filtered_df[filtered_df['LABEL'] == 9 ] #
	df_lay2sit_all = filtered_df[filtered_df['LABEL'] == 10 ] #
	df_sta2lay_all = filtered_df[filtered_df['LABEL'] == 11 ] #
	df_lay2stand_all = filtered_df[filtered_df['LABEL'] == 12 ] #


	df_transitions_all = pd.DataFrame(columns=['EXP_ID','USER_ID','LABEL','START','END'])
	df_transitions_all = pd.concat( [df_sta2sit_all,df_sit2sta_all,df_sit2lay_all,df_lay2sit_all, df_sta2lay_all, df_lay2stand_all	], ignore_index=True )

	dic_sizes = [[0,len(df_wal_all)],[1,len(df_sup_all)],[2,len(df_sdo_all)],[3,len(df_sit_all)],[4,len(df_sta_all)],[5,len(df_lay_all)],[6,len(df_transitions_all)]]
	print(dic_sizes)

	sizes = [len(df_wal_all),len(df_sup_all),len(df_sdo_all),len(df_sit_all),len(df_sta_all),len(df_lay_all),len(df_transitions_all)]	
	#print( "all: ", sizes, " - Min: ", min(sizes) )
	min_size = int(min(sizes))
	return min_size



#@param uuid: list of UUIDs.
def batch_generator(uuids,epochs):
	gt_df = pd.read_csv(ucihapt_datapath+"RawData/labels.txt",sep="\s",engine='python',names=['EXP_ID','USER_ID','LABEL','START','END'])
	#exclude other uuids
	print( gt_df.head() )
	filtered_df = pd.DataFrame(columns=['EXP_ID','USER_ID','LABEL','START','END'])
	for uuid in uuids:
		data_uuid = gt_df[ gt_df['USER_ID'] == uuid ]
		filtered_df = pd.concat([filtered_df,data_uuid], ignore_index=True)

	print( filtered_df.head() )
	#print("all data: ",len(gt_df))
	#print("only uuids: ", len(filtered_df))
	df_wal_all = filtered_df[filtered_df['LABEL'] == 1 ] #WALKING = 1
	df_sup_all = filtered_df[filtered_df['LABEL'] == 2 ] #
	df_sdo_all = filtered_df[filtered_df['LABEL'] == 3 ] #
	df_sit_all = filtered_df[filtered_df['LABEL'] == 4 ] #
	df_sta_all = filtered_df[filtered_df['LABEL'] == 5 ] #
	df_lay_all = filtered_df[filtered_df['LABEL'] == 6 ] #	
	df_sta2sit_all = filtered_df[filtered_df['LABEL'] == 7 ] #
	df_sit2sta_all = filtered_df[filtered_df['LABEL'] == 8 ] #
	df_sit2lay_all = filtered_df[filtered_df['LABEL'] == 9 ] #
	df_lay2sit_all = filtered_df[filtered_df['LABEL'] == 10 ] #
	df_sta2lay_all = filtered_df[filtered_df['LABEL'] == 11 ] #
	df_lay2stand_all = filtered_df[filtered_df['LABEL'] == 12 ] #
	df_transitions_all = pd.DataFrame(columns=['EXP_ID','USER_ID','LABEL','START','END'])
	df_transitions_all = pd.concat( [df_sta2sit_all,df_sit2sta_all,df_sit2lay_all,df_lay2sit_all, df_sta2lay_all, df_lay2stand_all	], ignore_index=True )

	dic_sizes = [[0,len(df_wal_all)],[1,len(df_sup_all)],[2,len(df_sdo_all)],[3,len(df_sit_all)],[4,len(df_sta_all)],[5,len(df_lay_all)],[6,len(df_transitions_all)]]
	print(dic_sizes)

	sizes = [len(df_wal_all),len(df_sup_all),len(df_sdo_all),len(df_sit_all),len(df_sta_all),len(df_lay_all),len(df_transitions_all)]	
	#print( "all: ", sizes, " - Min: ", min(sizes) )
	min_size = int(min(sizes))

	for epochs in range(epochs):
		#one epoch finish afer min_size samples per class
		for i in range(min_size):
			X_list = []
			y_list = []
			#print("preparing batch")
			#1 batch 1 segement per class
			# about 30x7=210 samples per batch
			######## ADDING SAMPLES to current batch#############			
			fragment = df_wal_all.sample(n=1)
			X_list.append( extract_data(fragment) )
			y_list.append(0)
			fragment = df_sup_all.sample(n=1)
			X_list.append( extract_data(fragment) )
			y_list.append(1)
			fragment = df_sdo_all.sample(n=1)
			X_list.append( extract_data(fragment) )
			y_list.append(2)
			fragment = df_sit_all.sample(n=1)
			X_list.append( extract_data(fragment) )
			y_list.append(3)
			fragment = df_sta_all.sample(n=1)
			X_list.append( extract_data(fragment) )
			y_list.append(4)
			fragment = df_lay_all.sample(n=1)
			X_list.append( extract_data(fragment) )
			y_list.append(5)
			fragment = df_transitions_all.sample(n=1)
			X_list.append( extract_data(fragment) )
			y_list.append(6)

			X_es = np.zeros((len(y_list),128,6))
			X_es[:,:] = [x for x in X_list  ]
			#Scaling input
			'''X_es_scaled = np.zeros((len(y_list),120,3))
			scaler_X = extrautils.load_classifier("./ACC_global_scaler_x")
			scaler_Y = extrautils.load_classifier("./ACC_global_scaler_y")
			scaler_Z = extrautils.load_classifier("./ACC_global_scaler_z")
			X_es_scaled[:,:,0] = scaler_X.transform(X_es[:,:,0])
			X_es_scaled[:,:,1] = scaler_Y.transform(X_es[:,:,1])
			X_es_scaled[:,:,2] = scaler_Z.transform(X_es[:,:,2])'''
			y_es = np.zeros(len(y_list))
			y_es[:] = [y for y in y_list]
			y_scaled = keras.utils.to_categorical(y_es, num_classes=7)
			yield (X_es, y_scaled)



def extract_data(fragment):
	X_point = np.zeros((128,6))
	exp_id = fragment.iloc[0]['EXP_ID']
	user_id = fragment.iloc[0]['USER_ID']
	start = fragment.iloc[0]['START']
	end = fragment.iloc[0]['END']
	#print(start," - ",end)
	if end > (start +128):
		rnd = randint(start,end-128)
	else:
		rnd = 0
	# filename acc: acc_expXX_userXX.txt
	# filename gyro: gyro_expXX_userXX.txt
	str_user_id = str(user_id)
	if user_id < 10:
		str_user_id = "0"+str(user_id)
	str_exp_id = str(exp_id)
	if exp_id < 10:
		str_exp_id = "0"+str(exp_id)
	accfile = ucihapt_datapath+"RawData/acc_exp"+str_exp_id+"_user"+str_user_id+".txt"
	gyrfile = ucihapt_datapath+"RawData/gyro_exp"+str_exp_id+"_user"+str_user_id+".txt"
	acc_data_df = pd.read_csv(accfile,names=['x','y','z'],sep='\s|,', engine='python')
	gyr_data_df = pd.read_csv(gyrfile,names=['x','y','z'],sep='\s|,', engine='python')
	acc_x = acc_data_df['x'].values
	acc_y = acc_data_df['y'].values
	acc_z = acc_data_df['z'].values
	gyr_x = gyr_data_df['x'].values
	gyr_y = gyr_data_df['y'].values
	gyr_z = gyr_data_df['z'].values
	
	X_point[:,0] = acc_x[rnd:rnd+128]
	X_point[:,1] = acc_y[rnd:rnd+128]			
	X_point[:,2] = acc_z[rnd:rnd+128]
	X_point[:,3] = gyr_x[rnd:rnd+128]
	X_point[:,4] = gyr_y[rnd:rnd+128]
	X_point[:,5] = gyr_z[rnd:rnd+128]
	return X_point


#retrive all data for a list of users
# Used for generating validation and test set
def get_all_data(uuids):
	gt_df = pd.read_csv(ucihapt_datapath+"RawData/labels.txt",sep="\s",names=['EXP_ID','USER_ID','LABEL','START','END'],engine='python')
	#exclude other uuids
	print( gt_df.head() )
	filtered_df = pd.DataFrame(columns=['EXP_ID','USER_ID','LABEL','START','END'])
	for uuid in uuids:
		data_uuid = gt_df[ gt_df['USER_ID'] == uuid ]
		filtered_df = pd.concat([filtered_df,data_uuid], ignore_index=True)

	X_list = []
	y_list = []
	for index, row in filtered_df.iterrows():
		exp_id = row['EXP_ID']
		user_id = row['USER_ID']
		start = row['START']
		end = row['END']
		label = row['LABEL']
		str_user_id = str(user_id)
		if user_id < 10:
			str_user_id = "0"+str(user_id)
		str_exp_id = str(exp_id)
		if exp_id < 10:
			str_exp_id = "0"+str(exp_id)
		accfile = ucihapt_datapath+"RawData/acc_exp"+str_exp_id+"_user"+str_user_id+".txt"
		gyrfile = ucihapt_datapath+"RawData/gyro_exp"+str_exp_id+"_user"+str_user_id+".txt"
		#print(accfile)
		acc_data_df = pd.read_csv(accfile,names=['x','y','z'],sep='\s|,', engine='python')
		gyr_data_df = pd.read_csv(gyrfile,names=['x','y','z'],sep='\s|,', engine='python')
		acc_x = acc_data_df['x'].values
		acc_y = acc_data_df['y'].values
		acc_z = acc_data_df['z'].values
		gyr_x = gyr_data_df['x'].values
		gyr_y = gyr_data_df['y'].values
		gyr_z = gyr_data_df['z'].values

		until = start + 128
		while until < end:
			X_point = np.zeros((128,6))
			X_point[:,0] = acc_x[start:until]
			X_point[:,1] = acc_y[start:until]			
			X_point[:,2] = acc_z[start:until]
			X_point[:,3] = gyr_x[start:until]
			X_point[:,4] = gyr_y[start:until]
			X_point[:,5] = gyr_z[start:until]
			X_list.append(X_point)
			if label < 7:
				y_list.append(label-1)
			else:
				y_list.append(6)
			start += 64
			until += 64
	X_es = np.zeros((len(y_list),128,6))
	X_es[:,:] = [x for x in X_list  ]
	y_es = np.zeros(len(y_list))
	y_es[:] = [y for y in y_list]
	y_scaled = keras.utils.to_categorical(y_es, num_classes=7)
	return (X_es, y_scaled)