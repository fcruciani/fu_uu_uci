import pandas as pd 
import matplotlib
import matplotlib.pyplot as plt


font = {'family':'sans-serif', 'size':16}
matplotlib.rc('font',**font)

train_history_file = "./keras_logs/HAR_IMU_3-CNN_k32_SGD_dropout__training_log.csv"

history_df = pd.read_csv(train_history_file)

print(history_df.head()) 

fig = plt.figure(figsize=(10, 8))
plt.plot(history_df['acc'].values,linewidth=3.0)
plt.plot(history_df['val_acc'].values,linewidth=3.0)
plt.title('Model accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Training set', 'Validation set'], loc='upper left')
fig.savefig("train_history.png",dpi=300)
plt.show()