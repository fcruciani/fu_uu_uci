import numpy as np
import pandas as pd 
from os.path import expanduser
#get actual home path for current user
home = expanduser("~")


'''
1 WALKING
2 W_UPSTAIRS
3 W_DOWNSTAIRS
4 SITTING
5 STANDING
6 LAYING
7 STAND_TO_SIT
8 SIT_TO_STAND
9 SIT_TO_LIE
10 LIE_TO_SIT
11 STAND_TO_LIE
12 LIE_TO_STAND
'''

ucihapt_datapath = home+"/python/data/HAPT_Dataset/"


def get_test_uuids():
	test_uuids = pd.read_csv(ucihapt_datapath+"Test/subject_id_test.txt",names=["UUID"])
	all_test_uuids = np.unique(test_uuids.values)
	return all_test_uuids

def get_train_uuids():
	train_uuids = pd.read_csv(ucihapt_datapath+"Train/subject_id_train.txt",names=["UUID"])
	all_train_uuids = np.unique(train_uuids.values)
	return all_train_uuids






