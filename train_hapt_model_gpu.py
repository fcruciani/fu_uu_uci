import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
from keras.utils import to_categorical
import matplotlib.pyplot as plt
import matplotlib
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support, classification_report, accuracy_score
#local imports
import Classifiers
import HAPT_Dataset as UCI_HAPT
from HAPT_Dataset import ucihapt_datapath
from os.path import expanduser
import hapt_imu_batch_generator as imu_bg

#get actual home path for current user
home = expanduser("~")

#Utility Functions
def printClassificationReport(predictions,true,classes,filename):
	cr = classification_report(np.array(true), np.array(predictions),target_names=classes,digits=4)
	print(cr)
	if not filename == "":
		with open(filename,"w") as out_file:
			out_file.write(cr)

def plotConfusionMatrix(predictions,true,classes,saveFig=True,showGraph=False,filename="undefined"):
		cm = confusion_matrix(np.array(true), np.array(predictions) )
		fig = plt.figure(figsize=(8, 7))
		cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
		df_cm = pd.DataFrame(cm,index=classes,columns=classes)
		ax = sn.heatmap(df_cm,cmap='Blues',annot=True)
		plt.yticks(rotation=0)
		if showGraph:
			plt.show()
		if saveFig:
			fig.tight_layout()
			if filename == "undefined":
				fig.savefig(self.name+"_CM.png",dpi=300)
			else:
				fig.savefig(filename,dpi=300)

def printBalancedAccuracyScore(pred, true,filename=""):
		bal_acc = balanced_accuracy_score(np.array(true), np.array(pred))
		print("Balanced Accuracy: ",acc)
		if not filename == "":
			with open(filename,"w") as out_file:
				out_file.write(str(acc))



train_uuids = UCI_HAPT.get_train_uuids()
test_uuids = UCI_HAPT.get_test_uuids()

tr_uuids = train_uuids[0:18]
vl_uuids = train_uuids[18:21]

print("Loading training dataset.")
(X_train,y_train) = imu_bg.get_all_data(train_uuids)
print("Loading validation dataset.")
#(X_vld,y_vld) = imu_bg.get_all_data(vl_uuids)
(X_vld,y_vld) = imu_bg.get_all_data(test_uuids)


classes = ["WALKING", "W. UPSTAIRS", "W. DOWNSTAIRS", "SITTING", "STANDING", "LAYING","TRANSITION"]


clf = Classifiers.HAPT_IMU_CNN_gpu(patience=500,layers=3,kern_size=32,divide_kernel_size=True)
#uuids,epochs, val_data, imu_bg):
class_weights = {0:1.,1:1.,2:1.,3:1.,4:1.,5:1.,6:10.}
clf.fit_class_weights(X_train,y_train,X_vld,y_vld,batch_size=32,class_weights=class_weights,epochs=10000)
#clf.fit_gen(uuids=tr_uuids,epochs=10000,val_data=(X_vld,y_vld),imu_bg=imu_bg)

