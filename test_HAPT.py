import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
from keras.utils import to_categorical
import matplotlib.pyplot as plt
import matplotlib
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support, classification_report, balanced_accuracy_score
#local imports
import Classifiers
import HAPT_Dataset as UCI_HAPT
from HAPT_Dataset import ucihapt_datapath
from os.path import expanduser
import hapt_imu_batch_generator as imu_bg
import seaborn as sn
#get actual home path for current user
home = expanduser("~")

font = {'family':'sans-serif', 'size':12}
matplotlib.rc('font',**font)



#Utility Functions
def printClassificationReport(predictions,true,classes,filename):
	cr = classification_report(np.array(true), np.array(predictions),target_names=classes,digits=4)
	print(cr)
	if not filename == "":
		with open(filename,"w") as out_file:
			out_file.write(cr)

def plotConfusionMatrix(predictions,true,classes,saveFig=True,showGraph=False,filename="undefined",normalize=True):
		cm = confusion_matrix(np.array(true), np.array(predictions) )
		fig = plt.figure(figsize=(10, 8))
		if normalize == True:
			cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
		df_cm = pd.DataFrame(cm,index=classes,columns=classes)
		ax = sn.heatmap(df_cm,cmap='Blues',annot=True)
		plt.yticks(rotation=0)
		if showGraph:
			plt.show()
		if saveFig:
			fig.tight_layout()
			if filename == "undefined":
				fig.savefig(self.name+"_CM.png",dpi=300)
			else:
				fig.savefig(filename,dpi=300)

def printBalancedAccuracyScore(pred, true,filename=""):
		bal_acc = balanced_accuracy_score(np.array(true), np.array(pred))
		print("Balanced Accuracy: ",bal_acc)
		if not filename == "":
			with open(filename,"w") as out_file:
				out_file.write(str(bal_acc))



classes = ["WALKING", "W. UPSTAIRS", "W. DOWNSTAIRS", "SITTING", "STANDING", "LAYING","TRANSITION"]


clf = Classifiers.HAPT_IMU_CNN(patience=200,layers=3,kern_size=32,divide_kernel_size=True)
#Loading saved model
clf.loadBestWeights()


test_uuids = UCI_HAPT.get_test_uuids()
(X_test,y_test) = imu_bg.get_all_data(test_uuids)

y_true = [ [np.argmax(y)] for y in y_test]#one_hot(labels_test)



y_predictions = clf.predict(X_test,batch_size=1)
y_predictions_inv = [ [np.argmax(y)] for y in y_predictions]

printClassificationReport(true=y_true,predictions=y_predictions_inv,classes=classes,filename="IMU_CNN_classification_report.txt")
plotConfusionMatrix(true=y_true,predictions=y_predictions_inv,classes=classes,showGraph=False,saveFig=True,filename="IMU_CNN_ConfusionMatrix_normalized.png")
printBalancedAccuracyScore(true=y_true,pred=y_predictions_inv,filename="IMU_CNN_balabnced_accuracy.txt")
plotConfusionMatrix(true=y_true,predictions=y_predictions_inv,classes=classes,showGraph=False,saveFig=True,filename="IMU_CNN_ConfusionMatrix.png",normalize=False)



### TODO: LOAD RECONSTRUCTED SIGNAL
''' 
predictions_reconstructed = clf.predict(X_test_reconstructed,batch_size=1)
predictions_reconstructed_inv = [ [np.argmax(x)] for x in predictions]
'''


