import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
from keras.utils import to_categorical
import matplotlib.pyplot as plt
import matplotlib
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support, classification_report, balanced_accuracy_score
#local imports
import Classifiers
import UCI_HAR_Dataset as UCI_HAR
from os.path import expanduser
import seaborn as sn
#get actual home path for current user
home = expanduser("~")

#Utility Functions
def printClassificationReport(predictions,true,classes,filename):
	cr = classification_report(np.array(true), np.array(predictions),target_names=classes,digits=4)
	print(cr)
	if not filename == "":
		with open(filename,"w") as out_file:
			out_file.write(cr)

def plotConfusionMatrix(predictions,true,classes,saveFig=True,showGraph=False,filename="undefined"):
		cm = confusion_matrix(np.array(true), np.array(predictions) )
		fig = plt.figure(figsize=(8, 7))
		cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
		df_cm = pd.DataFrame(cm,index=classes,columns=classes)
		ax = sn.heatmap(df_cm,cmap='Blues',annot=True)
		plt.yticks(rotation=0)
		if showGraph:
			plt.show()
		if saveFig:
			fig.tight_layout()
			if filename == "undefined":
				fig.savefig(self.name+"_CM.png",dpi=300)
			else:
				fig.savefig(filename,dpi=300)

def printBalancedAccuracyScore(pred, true,filename=""):
		bal_acc = balanced_accuracy_score(np.array(true), np.array(pred))
		print("Balanced Accuracy: ",bal_acc)
		if not filename == "":
			with open(filename,"w") as out_file:
				out_file.write(str(bal_acc))


#IMPORTANT: change this to point UCI_HAR data path
ucihar_datapath = home+"/python/data/UCI_HAR_Dataset/"

classes = ["WALKING", "W. UPSTAIRS", "W. DOWNSTAIRS", "SITTING", "STANDING", "LAYING"]


clf = Classifiers.IMU_CNN(patience=200,layers=3,kern_size=32,divide_kernel_size=True)#Classifiers.Hybrid_3CNN_k32(name="3CNN_k32")
X_train, labels_train, list_ch_train = UCI_HAR.read_IMU_data(data_path=ucihar_datapath, split="train") # train
X_test, labels_test, list_ch_test = UCI_HAR.read_IMU_data(data_path=ucihar_datapath, split="test") # test
assert list_ch_train == list_ch_test, "Mistmatch in channels!"
X_train, X_test = UCI_HAR.standardize(X_train, X_test)
print("Data size:", len(X_train), " - ", len(X_train[0]))

#preparing train test labels
labels_test[:] = [ y -1 for y in labels_test ] #labels [1-6] -> [0-5]

y_test = to_categorical(labels_test,num_classes=6)#one_hot(labels_test)


#Loading best weights
clf.loadBestWeights()

predictions = clf.predict(X_test,batch_size=1)
predictions_inv = [ [np.argmax(x)] for x in predictions]

printClassificationReport(true=labels_test,predictions=predictions_inv,classes=classes,filename="IMU_CNN_classification_report.txt")
plotConfusionMatrix(true=labels_test,predictions=predictions_inv,classes=classes,showGraph=False,saveFig=True,filename="IMU_CNN_ConfusionMatrix.png")
printBalancedAccuracyScore(true=labels_test,pred=predictions_inv,filename="IMU_CNN_balabnced_accuracy.txt")





